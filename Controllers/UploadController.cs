﻿using Exercicio2.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Threading.Tasks;
using System.Net;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Exercicio2.Controllers
{
    public class UploadController : Controller

    {
        private Exercicio2Context db = new Exercicio2Context();

        // GET: Upload
        public ActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public ActionResult UploadFotos(int id)
        {
            ViewBag.ProdutoId = id;
            return View();
        }
        [HttpPost]
        public object UploadFotos(HttpPostedFileBase[] file, int ProdutoId)
        {
            string _img="";
            try
            {
                foreach (var item in file)
                {
                    if (item.ContentLength > 0)
                    {
                        //upload
                        _img = Path.GetFileName(item.FileName);
                        var _path = Path.Combine(Server.MapPath("/UploadedFotos/"), _img);
                        item.SaveAs(_path);

                        //montando o obj
                        var FotoNome = new Foto();
                        FotoNome.Nome = _img;
                        FotoNome.ProdutoId = ProdutoId;

                        // adicionando
                        db.Fotos.Add(FotoNome);
                    }
                }


                //salvando
                db.SaveChanges();


                ViewBag.Message = "Imagem Enviada!";
            }
            catch
            {
                ViewBag.Message = "Erro ao enviar imagem!";
            }

            return View();
        }
    }
}