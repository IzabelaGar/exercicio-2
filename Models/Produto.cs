﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Exercicio2.Models
{
    public class Produto
    {
        public int ProdutoId { get; set; }
        [Display(Name = "Nome do Produto")]
        public string Nome { get; set; }
        [Display(Name = "Preço")]
        public decimal Preco { get; set; }
        public int SubCategoriaId { get; set; }
        public virtual SubCategoria SubCategoria { get; set; }
        public virtual ICollection<Foto> Fotos { get; set; }
    }
}