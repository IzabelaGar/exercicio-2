﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Exercicio2.Models
{
    public class Categoria
    {
        public int CategoriaId { get; set; }

        [Display(Name = "Categoria")]
        public string NomeCategoria { get; set; }
        public virtual ICollection<SubCategoria> SubCategorias { get; set; }

    }
}