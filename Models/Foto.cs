﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Exercicio2.Models
{
    public class Foto
    {
        public int FotoId { get; set; }
        public int ProdutoId { get; set; }

        public string Nome { get; set; }
        public virtual Produto Produto { get; set; }
        public HttpPostedFileBase[] files { get; set; }
    }
}