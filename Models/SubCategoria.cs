﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Exercicio2.Models
{
    public class SubCategoria
    {

        public int SubCategoriaId { get; set; }

        public int CategoriaId { get; set; }

        [Display(Name = "Sub Categoria")]
        public string Nome { get; set; }

        [JsonIgnore]
        public virtual Categoria Categoria { get; set; }
        [JsonIgnore]
        public virtual ICollection<Produto> Produtos { get; set; }
    }
}